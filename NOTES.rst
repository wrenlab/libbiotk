Ubuntu dependencies
===================

- g++
- gcc
- cmake
- libcurl4-openssl-dev
- libarmadillo-dev
- libhts-dev
- openssl
- libleveldb-dev

Had to temporarily disable to get to compile:

- iridescent
- cli/knn
