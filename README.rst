=====================================
libbiotk - C++ bioinformatics library
=====================================

Overview
========

libbiotk has C++ classes and functions for several bioinformatics-related
tasks:

- Manipulating and analyzing high-throughput sequencing data and file formats
- Parsing various file formats for biomedical data, such as ontologies
- Biomedical text mining
- Stream-oriented processing of tabular datasets

Installation
============

Dependencies
------------

- a recent C++ compiler supporting C++14
- cmake

Required libraries:

- curl
- armadillo
- htslib
- openssl
- leveldb

Optional libraries:

- Apache clownfish
- Apache lucy

Compilation
-----------

Inside the libbiotk/ directory, run the following commands:

.. code-block:: bash
    
    $ mkdir build
    $ cd build

For a root install:

.. code-block:: bash

    $ cmake ..
    $ make install

For a non-root install:

.. code-block:: bash

    $ cmake -DCMAKE_INSTALL_PREFIX:PATH=$HOME/.local ..
    $ make install

Usage
=====
