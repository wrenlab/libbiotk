#include <BioTK.hpp>

using namespace BioTK;
using namespace std;

int main(int argc, char* argv[]) {
    bool output_path = false;
    bool verbose = false;

    int c;
    while ((c = getopt(argc, argv, "p")) != -1) {
        switch (c) {
            case 'p':
                output_path = true;
                break;
            case 'v':
                verbose = true;
                break;
        }
    }
    argv += optind - 1;
    url_t url = string(argv[1]);

    DownloadCache cache;

    if (verbose) {
        bool hit = cache.is_cached(url);
        if (!hit) {
            cerr << "Cache miss for URL: " << url << endl;
        }
    }

    if (output_path) {
        cout << cache.fetch_path(url) << endl;
    } else {
        std::ifstream file;
        cache.fetch(file, url);
        std::cout << file.rdbuf();
    }
}
