#include <map>
#include <string>
#include <cmath>

#include <BioTK.hpp>

using namespace std;

void usage(int rc=0) {
    cerr << "usage: todo" << endl;
    exit(rc);
}

const double TOLERANCE = 1e-6;

enum ComparisonType {
    GT, GTE, EQ, LT, LTE
};

map<string, ComparisonType> comparison_map = {
    {"gt", GT}, 
    {"gte", GTE},
    {"eq", EQ},
    {"lt", LT},
    {"lte", LTE}
};

struct comparison_t {
    ComparisonType type;
    size_t column;
    double constant;

    bool compare(double query) {
        switch (type) {
            case GT:
                return query > constant;
            case GTE:
                return query >= constant;
            case EQ:
                return abs(query - constant) < TOLERANCE;
            case LT:
                return query < constant;
            case LTE:
                return query <= constant;
        }
        cerr << "Programming error!" << endl;
        exit(1);
        return false;
    }
};

int main(int argc, char* argv[]) {
    ifstream input("/dev/stdin");

    string header, line;
    map<string, size_t> columns;

    // Map header fields to indices
    getline(input, header);
    auto hfields = BioTK::split(header);
    for (size_t i=0; i<hfields.size(); i++)
        columns[hfields[i]] = i;

    if ((argc - 1) % 3 != 0) {
        usage(1);
    }

    vector<comparison_t> comparisons;
    // Determine filters
    for (size_t i=1; i<argc; i+=3) {
        size_t c = columns[argv[i]];
        string key = BioTK::lowercase(string(argv[i+1]));
        if (comparison_map.find(key) == comparison_map.end())
            usage(1);
        ComparisonType t = comparison_map[key];
        double constant = atof(argv[i+2]);
        comparisons.push_back(comparison_t{t,c,constant});
    }

    cout << header << endl;
    while (getline(input, line)) {
        auto fields = BioTK::split(line);
        bool ok = true;
        for (auto& cmp : comparisons) {
            double query = atof(fields[cmp.column].c_str());
            if (!cmp.compare(query)) {
                ok = false;
                break;
            }
        }
        if (ok)
            cout << line << endl;
    }
}
