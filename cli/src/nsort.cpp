#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include <unistd.h>

#include <BioTK.hpp>

using namespace std;

vector<size_t> 
sort_string(vector<string>& lines, size_t column) {
    typedef pair<string, size_t> ix_t;
    vector<ix_t> pairs;
    for (size_t i=0; i<lines.size(); i++) {
        auto fields = BioTK::split(lines[i]);
        pairs.push_back(ix_t(fields[column], i));
    }
    sort(pairs.begin(), pairs.end());

    vector<size_t> o;
    for (auto& pair : pairs)
        o.push_back(pair.second);
    return o;
}

vector<size_t> 
sort_numeric(vector<string>& lines, size_t column) {
    typedef pair<double, size_t> ix_t;
    vector<ix_t> pairs;
    for (size_t i=0; i<lines.size(); i++) {
        auto fields = BioTK::split(lines[i]);
        pairs.push_back(ix_t(atof(fields[column].c_str()), i));
    }
    sort(pairs.begin(), pairs.end());

    vector<size_t> o;
    for (auto& pair : pairs)
        o.push_back(pair.second);
    return o;
}

void error(const char* msg) {
    cerr << "ERROR: " << msg << endl;
    exit(1);
}

void usage(int rc=0) {
    cerr << "usage: todo" << endl;
    exit(rc);
}

int main(int argc, char* argv[]) {
    int c;
    bool reverse = false;
    bool numeric = false;
    bool show_usage = false;
    size_t column = 0;
    ifstream input("/dev/stdin");

    while ((c = getopt(argc, argv, "hrn")) != -1) {
        switch (c) {
            case 'r':
                reverse = true;
                break;
            case 'n':
                numeric = true;
                break;
            case 'h':
                show_usage = true;
                break;
        }
    }

    if (show_usage)
        usage(0);

    string header;

    if (optind == (argc - 1)) {
        getline(input, header);
        auto fields = BioTK::split(header);
        string key(argv[argc-1]);
        column = find(fields.begin(), fields.end(), key) - fields.begin();
        if (column >= fields.size()) {
            error("requested column name not found");
        }
    } else if (optind == argc) {
        // "column" defaults to first
        getline(input, header);
    } else {
        // error: More than one positional
        error("more than one positional argument supplied -- only zero or one supported");
    }

    cout << header << endl;

    string line;
    vector<string> lines;
    while (getline(input, line)) {
        lines.push_back(line);
    }

    vector<size_t> sort_index;
    if (numeric) {
        sort_index = sort_numeric(lines, column);
    } else {
        sort_index = sort_string(lines, column);
    }

    if (reverse) {
        std::reverse(sort_index.begin(), sort_index.end());
    }

    for (auto& ix : sort_index) {
        cout << lines[ix] << endl;
    }
}
