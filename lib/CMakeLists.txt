file(GLOB_RECURSE SOURCES 
    "src/*.cpp")

add_library(biotk SHARED ${SOURCES})

set_target_properties(biotk PROPERTIES 
    VERSION ${BioTK_VERSION_STRING}
    LINKER_LANGUAGE CXX)

target_link_libraries(biotk 
    z
    armadillo
    config++
    curl
    hts
    leveldb
    ssl
)

##############
# Installation
##############

install(TARGETS biotk
        LIBRARY DESTINATION lib
)

install(DIRECTORY include/
    DESTINATION include
    FILES_MATCHING PATTERN "*.hpp"
)
