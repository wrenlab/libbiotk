#pragma once

#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <string>
#include <vector>
#include <cstddef>
#include <memory>
#include <exception>
#include <stdexcept>
#include <map>

#include <armadillo>
#include <glog/logging.h>

namespace BioTK {

typedef std::string path_t;
typedef std::string url_t;

typedef size_t gene_id_t;
typedef size_t taxon_t;

};
