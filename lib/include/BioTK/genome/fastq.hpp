#include <string>
#include <fstream>
#include <memory>

namespace BioTK {

struct FASTQ {
    std::string name, sequence, quality;
};

class FASTQFile {
    std::ifstream handle;
    bool _eof = false;

public:
    FASTQFile(std::string path) : 
        handle.open(path.c_str());
    }

    ~FASTQFile() {
        handle.close();
    }

    bool eof() {
        return _eof;
    }

    std::shared_ptr<FASTQ> next() {
        std::string ignore;

        if (_eof)
            return NULL;

        FASTQ fq;
        if (!getline(handle, fq.name)) {
            _eof = true;
            return NULL;
        }
        fq.name = fq.name.substr(1);
        getline(handle, fq.sequence);
        getline(handle, ignore);
        getline(handle, fq.quality);
        return std::make_shared<FASTQ>(fq);
    }
};

}
