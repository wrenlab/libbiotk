#pragma once

#include <string>

namespace BioTK {
namespace base64 {

std::string encode(std::string);
std::string decode(std::string);

};
};
