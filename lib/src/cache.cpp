#include "BioTK/cache.hpp"
#include "BioTK/util/base64.hpp"
#include "BioTK/util.hpp"
#include "BioTK/ext/gzstream.hpp"

using namespace std;

namespace BioTK {

/* Helpers */

string
cache_path(string cachedir, url_t url) {
    string key = BioTK::base64::encode(lowercase(url));
    return cachedir + "/" + key;
}

/* Class methods */

bool 
DownloadCache::is_cached(url_t url) {
    return path_exists(cache_path(dir, url));
}

string
DownloadCache::fetch_path(url_t url) {
    string path = cache_path(dir, url);
    if (!path_exists(path)) {
        download(url, path);
    }
    return path;
}

void 
DownloadCache::fetch(ifstream& handle, url_t url) {
    path_t path = fetch_path(url);
    handle.open(path);
}
 
}; // ns BioTK
